﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoDDD.Application.Interface;
using ProjetoDDD.Domain.Entities;
using ProjetoDDD.Domain.Interfaces.Services;
using ProjetoDDD.Domain.Services;

namespace ProjetoDDD.Application
{
    public class ClienteAppService : AppServiceBase<Cliente>, IClienteAppService
    {
        private readonly IClienteService _clienteservice;

        public ClienteAppService(IClienteService clienteService) : base(clienteService)
        {
            _clienteservice = clienteService;
        }

        public IEnumerable<Cliente> ObterClientesEspeciais()
        {
            return _clienteservice.ObterClientesEspeciais(_clienteservice.GetAll());
        }
    }
}
